<?php
 

 
require(APPPATH.'libraries/REST_Controller.php');

class Persons extends REST_Controller {
	
	//https://wete-meri-alho-merial-1.c9users.io/WETE/CodeIgniter-3.0.6/index.php/Persons/list
	
	// GET method implementation for .../Persons/list
	function list_get() {

		// establish connection to MongoDB server
		$connection = new MongoClient();

		// select database
		$db = $connection->company;

		// select collection 
		$collection = $db->persons;

		// retrieve all items in collection
		$cursor = $collection->find();

		if($cursor->hasNext())
		{
    		echo json_encode(iterator_to_array($cursor));
		}	
		
		// close connection
		$connection->close();
	}
	
	//https://wete-meri-alho-merial-1.c9users.io/WETE/CodeIgniter-3.0.6/index.php/Persons/person?firstname=x&lastname=y&id=z&salary=n
	// GET method implementation for .../Persons/person?firstname=x&lastname=y&id=z&salary=n
	function person_get(){
    	
    	// establish connection to MongoDB server
    	$connection = new MongoClient();
    	
    	// select database
		$db = $connection->company;
		
		// select collection 
		$collection = $db->persons;
		
		$id = $this->get('id');
		$firstname = $this->get('firstname');
		$lastname = $this->get('lastname');
		$salary = $this->get('salary');
		
		$person = array("id" => $id, "first name" => $firstname, "last name" => $lastname, "salary" => $salary);

    	$db->persons->insert($person);
    	echo "ADDED: " . json_encode($person);
    	
    }
}

?>
