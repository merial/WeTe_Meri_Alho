<?php
    class Chat  extends CI_Controller{
        private $model;

        public function __construct() {
            parent::__construct();
            $this->load->model('Chat_model');
            $this->load->helper('url_helper');
        }

        public function index() {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->view('chat/view');
        }

        
        public function send() {
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            if ($this->form_validation->run() == FALSE){
                $this->load->view('chat/view');
            }
            else{
                
                $this->load->view('chat/view');
            }
                        
            
            $this->form_validation->set_rules('message', 'Message', 'required');
            
            
            
            $this->chat_model->add_message($_POST["nickname"], $_POST["message"]);
            $this->load->view('chat/view');
        }
        


    }
