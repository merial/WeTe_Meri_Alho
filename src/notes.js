(function(window) {
    var notes = [];
    var i = 0;

    function Notes() {

    }

    Notes.prototype.add = function(note) {
        var dt = new Date();
        var utcDate = dt.toUTCString();

        if (note && note.match(/\S/)) {

            var item = {
                note: note,
                time: utcDate
            };
            notes.push(item);

            return true;
        }
        return false;
    }


    Notes.prototype.remove = function(index) {
        if (index <= notes.length) {
            notes.splice(index);
            return true;
        }
        else {
            return false;
        }

    }


    Notes.prototype.count = function() {
        return notes.length;
    }


    Notes.prototype.list = function() {
        return notes;
    }


    Notes.prototype.find = function(string) {
        var containsarr = [];
        var i = 0;
        for (var j = 0; j < notes.length; j++) {
            if (notes.indexOf(string)) {
                containsarr[i] = notes[j];
                i++;
            }
            
        }
        if (containsarr!=null){
            return true;
        }
        return false;
    }


    Notes.prototype.clear = function() {
        notes.splice(0, notes.length);
    }


    window.app = window.app || {};
    window.app.Notes = Notes;
})(window)
