<?php
// follow gethint.php
// get the q parameter from URL
 $q = $_REQUEST["location"];
// change the following
$hint = "";
$apiKey = "2ca1eeaf6b7845fcbf5b48f79200f7c9";

//if ($q !== ""){
  /**
   * Here we build the url we'll be using to access the google maps api
   */
  
  $maps_url = 'https://'.
  'maps.googleapis.com/'.
  'maps/api/geocode/json'.
  '?address=' . urlencode($_GET['location']);
  $maps_json = file_get_contents($maps_url);
  $maps_array = json_decode($maps_json, true);
  $lat = $maps_array['results'][0]['geometry']['location']['lat'];

  $lng = $maps_array['results'][0]['geometry']['location']['lng'];
  
  /**
   * Time to make our Instagram api request. We'll build the url using the
   * coordinate values returned by the google maps api
   */
  $breezometer_url = 'https://'.
  'api.breezometer.com/baqi/'.
  '?lat='.$lat.
  '&lon='.$lng.
  '&key='.$apiKey ;
  $breezometer_json = file_get_contents($breezometer_url);
  $breezometer_array = json_decode($breezometer_json, true);
  
 
 // give it back to Javascript
 echo $breezometer_json;