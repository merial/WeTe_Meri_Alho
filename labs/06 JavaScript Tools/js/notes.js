'use strict';
var notes = new Array();
var textbox;
var button;

loadList();

function addItem() {
	textbox = document.getElementById('item');
	var itemText = textbox.value;
	textbox.value = '';
	textbox.focus();
	var newItem = {
		title: itemText,
		quantity: 1
	};


	for (var i = 0; i < notes.length; i++) {
		var noteA = notes[i];

		if (noteA.title == newItem.title) {

			noteA.quantity += 1;
			deleteIndex(i);


			newItem.title = noteA.title;
			newItem.quantity = noteA.quantity;
		}


	}
	notes.push(newItem);
	saveList();
	displayList();

}

function displayList() {
	var table = document.getElementById('list');
	table.innerHTML = '';
	for (var i = 0; i < notes.length; i++) {
		var node = undefined;
		var note = notes[i];
		var node = document.createElement('tr');
		var html = '<td>' + note.title + '</td><td>' + note.quantity + '</td><td><a href="#" onClick="deleteIndex(' + i + ')">delete</td>';
		node.innerHTML = html;
		table.appendChild(node);
	}
}

function deleteIndex(i) {
	notes.splice(i, 1);
	saveList();
	displayList();
}

function saveList() {
	/* global localStorage*/
	localStorage.notes = JSON.stringify(notes);
}

function loadList() {
	console.log('loadList');
	if (localStorage.notes) {
		notes = JSON.parse(localStorage.notes);
		displayList();
	}
}

button = document.getElementById('add');
button.onclick = addItem;
