// tasks.js
// This script manages a to-do list.

// Need a global variable:
var tasks = [];

// For the output:
var message = '';

// Function called when the form is submitted.
// Function adds a task to the global array.
function addTask() {
    'use strict';

    // Get the task:
    var task = document.getElementById('task');

    // Reference to where the output goes:
    var output = document.getElementById('output');



    if (task.value) {

        // Add the item to the array:
        tasks.push(task.value);

        // Update the page:
        message = '<h2>To-Do</h2><ol>';
        for (var i = 0, count = tasks.length; i < count; i++) {
            message += '<li>' + tasks[i] + '</li>';
        }
        message += '</ol>';
        output.innerHTML = message;

    } // End of task.value IF.

    // Return false to prevent submission:
    return false;

} // End of addTask() function.

var comp;

function removeDuplicates() {
    'use strict';

    // Get the task:
    var remove = document.getElementById('removedup');

    // Reference to where the output goes:
    var output = document.getElementById('output');

    var listitem;
    var compareitem;



    //go through the list to remove duplicates
    for (var i = 0, count = tasks.length; i < count; i++) {
        listitem = tasks[i];
        var j = 0;

        for (var i=0; j < tasks.length; j++ ) {
    
            compareitem = tasks[j];
            
            if (i==j){}
            if (listitem == compareitem) {
                tasks.splice(j, 1);

            }
       
        }

    }

    

    // Update the page:
    message = '<h2>To-Do</h2><ol>';
    for (var i = 0, count = tasks.length; i < count; i++) {
        message += '<li>' + tasks[i] + '</li>';
    }
    message += '</ol>';
    output.innerHTML = message;
} // End of removeDuplicates() function




// Initial setup:
function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = addTask;
    document.getElementById('removedup').onsubmit = removeDuplicates();

} // End of init() function.
window.onload = init;
